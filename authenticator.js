"use strict";
const { promises: fs } = require("fs");
const { v4: uuid } = require('uuid');
const crypt = require("bcryptjs");

let UserType = {
	Admin: 1,
	Host: 2,
	Node: 3
}

class Token{
	constructor(id=uuid(), ts=Date.now()){
		this.m_id = id;
		this.m_ts = ts;
	}
	get id() { return this.m_id; }
	get ts() { return this.m_ts; }
	serialize(){
		let data = {
			id: this.m_id,
			ts: this.m_ts
		}
		return data;
	}	
}

class User{
	constructor(id, pwhash, type){
		this.m_id = id;
		this.m_pwhash = pwhash; 
		this.m_type = type;
		this.m_tokens = {};
	}
	get id() { return this.m_id; }
	get Type() { return UserType; }
	get type() { return this.m_type; }
	set type(value) { 
 		this.m_type = value;
 		this.changed(this);
  }
  
	authenticate(pw){
		if (crypt.compare(pw, this.m_pwhash)){
			let token = new Token();
			this.m_tokens[token.id] = token;
			this.changed(this);
			return token;
		} else {
			return null;	
		}
	}
	checkToken(token){
		return token in this.m_tokens;
	}
	changed(user){}
	deSerialize(data){
		this.m_id = data.id;
		this.m_pwhash = data.pwhash;
		this.type = data.type;
		for(let i in data.tokens){
			let td = data.tokens[i];
			this.m_tokens[td.id] = new Token(td.id, td.ts);
		}
	}
	serialize(){
		let data =  { 
			id: this.m_id, 
			pwhash: this.m_pwhash, 
			type: this.m_type,
			tokens: []
		}
		for (let i in this.m_tokens){
			data.tokens.push(this.m_tokens[i].serialize());
		}
		return data;
	}
}

class Authenticator{
	constructor(config, app){
		this.m_config = config;
		this.m_users = {};
		this.m_filePath = config.appDataPath + "/auth.json";
		this.m_app = app;
		this.loadAuthData(config);
		app.post("/ws/auth", (req, res) => this.auth(req, res) );
	}
	async loadAuthData(config){
		try {
			let data = JSON.parse(await fs.readFile(this.m_filePath));
			this.deSerialize(data);
			let admin = new User(config.admin.id, config.admin.pwhash, UserType.Admin);
			admin.changed = (user) => this.onUserChanged(user);
			this.m_users[admin.id] = admin;
		} catch (e){
			console.log(e);
		}
	}
	checkToken(userId, token){
		let user = this.m_users[userId];
		if (user){
		 	return user.checkToken(token);
		}
		return false;
	}
	auth(req, res){
		let id = req.body.id;
		let pw = req.body.pw;
		let token = this.authenticate(id, pw);
		if (token) {
			res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});      
      return res.end(JSON.stringify(token.serialize()));
		} else {
			res.writeHead(401, {'Content-Type': 'html/text; charset=utf-8'});      
      return res.end("Access denied");
		}
	}
	authenticate(id, pw){
		if (id in this.m_users){
			return this.m_users[id].authenticate(pw);
		} else return null;
	} 
	async createUser(pw, type){
		let user = new User(uuid(), await crypt.hash(pw, 12), type);
		this.m_users[user.id] = user;
		user.changed = (user) => this.onUserChanged(user);
		await this.saveAuthData();
		return user;
	}
	onUserChanged(user){
		this.saveAuthData();
	}
	async saveAuthData(){
		let data = this.serialize();
		let txt = JSON.stringify(data);
		await fs.writeFile(this.m_filePath, txt);
	}
	deSerialize(data){
		for (let i in data.users){
			let userData = data.users[i];
			let user = new User(userData.id, userData.pwhash, userData.type);
			user.deSerialize(userData);
			user.changed = (user) => this.onUserChanged(user);
			this.m_users[user.id] = user;
		} 
	}
	serialize(){
		let data = {
			users: []
		}
		for (let i in this.m_users){
			let user = this.m_users[i].serialize();
			data.users.push(user); 
		}
		return data;
	}
}
 
module.exports = Authenticator;
