#!/usr/bin/env node
"use strict"; 
const fs = require('fs');
const express = require('express');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const WebSocketServer = require("./websocketserver.js");

let app = express();
let config = null;
let appDataPath = process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + "/.local/share");
appDataPath += "/GenericWebSocketClient";
let configFile = appDataPath + "/config.json";

if (!fs.existsSync(configFile)) {
	if (!fs.existsSync(appDataPath)){ 
		fs.mkdirSync(appDataPath);
	}
	fs.copyFileSync("./defaultConfig.json", configFile);
	console.log("new config file written to: " + configFile);
}

if (fs.existsSync(configFile)) {
	var data = fs.readFileSync(configFile, 'utf8');
  config = JSON.parse(data);
} else {
	console.log("no config file");
  process.exit(0);
}

 
config.appDataPath = appDataPath;

for (let i in process.argv){
  var arg = process.argv[i];
  if (arg.includes("debug")) config.debug = true;
}

let server = app.listen(config.port, function () {
  console.log('Generic websocket server listening on port ' + config.port + '!')
});

app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static('./public'));

let wss = new WebSocketServer(server, app, config);
 
