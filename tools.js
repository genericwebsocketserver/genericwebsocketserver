"use strict";

class Tools{
	constructor(config){
		this.m_config = config;
	}
	log(msg){
		console.log(msg);
	}
	debug(msg){
		if (this.m_config.debug) console.log(msg);
	}
	error(msg){
		console.error(msg);
	}
}

module.exports = Tools;
