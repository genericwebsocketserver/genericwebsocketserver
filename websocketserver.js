"use strict";
const ws = require('ws');
const { v4: uuid } = require('uuid');
const Authenticator = require("./authenticator.js");
const Tools = require("./tools"); 

let MessageType = {
	ClientAdded: 1,
	ClientRemoved: 2,
	Init: 3,
	SendAll: 4,
	SendTo: 5,
	Error: 6,
	ChangeSector: 7,
	CreateSector: 8,
	CloseSector: 9,
	Kick: 10,
	AccessDenied: 11
}

let tools = null;

class WebSocketHandler{
	constructor(gwss, ws, sector){
		this.m_gwss = gwss;
		this.m_ws = ws;
		this.m_id = uuid();
		this.m_sector = sector;
		ws.on("message", (msg) => this.onMessage(msg));
		ws.on("close", (e) => this.onClose(e));
		sector.addHandler(this);
	}
	async onMessage(msg){
		if (typeof msg === "object"){
		 	msg = msg.toString();
		}  
		this.handleMessage(msg);
	}
	close(){
		this.m_ws.close();
	}
	handleMessage(msg){
		try{
			let json = JSON.parse(msg);
			json.sender = this.id;
			switch (json.t){
				case MessageType.SendTo:
					this.m_sector.sendTo(json.id, JSON.stringify(json));
					break;
				case MessageType.SendAll:
					this.m_sector.sendAll(this.id, JSON.stringify(json));
					break;
				case MessageType.ChangeSector:
					this.m_gwss.handleChangeSector(this, json);
					break;
				case MessageType.CreateSector:
					this.m_gwss.handleCreateSector(this, json);
					break;
				case MessageType.Kick:
					this.m_sector.handleKick(this, json);
					break;
				default:
					tools.debug(msg);
					this.sendError(msg);
					break;
			}
		} catch (e){
			console.log(e);
			this.sendError(msg);
		}
	}
	onClose(e){
		tools.debug(e);
		this.m_sector.onHandlerClosed(this);
		this.m_gwss.onHandlerClosed(this);
	}
	send(msg){
		this.m_ws.send(msg);
	}
	sendError( msg){
		let errorMsg = {
			t: MessageType.Error,
			msg: msg
		}
		this.send(JSON.stringify(errorMsg));
	}
	sendAccessDenied(){
		this.send(JSON.stringify({ t: MessageType.AccessDenied }));
	}
	get id(){ 
		return this.m_id;
	}  
	get sector() { return this.m_sector; }
	set sector(value) { 
		if (!value)	throw new Error("sector is null");
		this.m_sector.removeHandler(this);
		this.m_sector = value; 
		this.m_sector.addHandler(this);
	}
}

class WebSocketSector{
	constructor(id, host){
		this.m_id = id;
		this.m_handlers = {}
		this.m_host = host;
	}
	get id() { return this.m_id; }
	sendAll(sender, msg){
		for (let i in this.m_handlers){
			let h = this.m_handlers[i];
			if (h.id != sender){
				h.send(msg);
			} 
		}
	} 
	sendTo(id, msg){
		let handler = this.m_handlers[id].send(msg); 
	}
	sendInit(handler){
		let initMsg = {
			t: MessageType.Init,
			id: handler.id,
			sectorId: this.m_id,
			clients: [],
			hostId: this.m_host.id
		}
		for (let i in this.m_handlers){
			let h = this.m_handlers[i];
			initMsg.clients.push(h.id);
		} 
		handler.send(JSON.stringify(initMsg));
	}
	addHandler(handler){
		this.m_handlers[handler.id] = handler;
		this.sendInit(handler);
    this.sendAll(handler.id, JSON.stringify({t:MessageType.ClientAdded, id: handler.id}));
		tools.debug("handler: " + handler.id + " added to sector: " + this.id);
	}
	removeHandler(handler){
		delete this.m_handlers[handler.id];
		let msg = {
			t: MessageType.ClientRemoved,
			id: handler.id
		}
		this.sendAll(handler.id, JSON.stringify(msg));
		if (Object.keys(this.m_handlers).length == 0) {
			this.sectorEmpty(this);
		} else {
			if (this.m_host.id == handler.id){
				tools.debug("host left closing sector!");
				for (let i in this.m_handlers){
					let h = this.m_handlers[i];
					h.close();
				}
			}
		}
	}
	sectorEmpty(sector){
		
	}  
	onHandlerClosed(handler){
		this.removeHandler(handler);
	}
	handleKick(sender, json){
		if (sender == this.m_host){
			let guestId = json.id;
			if (guestId in this.m_handlers){
				let guestHandler = this.m_handlers[guestId];
				guestHandler.send(JSON.stringify(json));
				guestHandler.close(); 
			}	else {
				this.m_host.sendError("Guest not found");
			}
		} else {
			sender.sendError("Only host can kick guests");
		}
	}
}

class WebSocketLobby {
	constructor(){
		this.m_handlers = {};	
	}
	addHandler(handler){
		this.m_handlers[handler.id] = handler;
		setTimeout(()=>this.onTimeout(handler.id), 1000);
	}
	onTimeout(handlerId){
		if (handlerId in this.m_handlers){
			let handler = this.m_handlers[handlerId];
			handler.sendError("No sector assigned.");
			handler.close();
		}
	}
	removeHandler(handler){
		delete this.m_handlers[handler.id];
	}
	onHandlerClosed(handler){
		tools.debug("handler " + handler.id + " kicked from lobby");
	}
	sendAll(){}
}

class WebSocketServer{
	constructor(server, app, config){
		this.m_app = app;
		this.m_config = config;
		this.m_server = server;
		this.m_lobby = new WebSocketLobby();
		this.m_sectors = {}
		this.m_handlers = {}
		this.m_auth = new Authenticator(config, app);
		this.m_wss = new ws.Server({
      server: server,
      path: "/ws"
    });
    this.m_wss.on("connection", (webSocket) => this.onConnection(webSocket)); 
    tools = new Tools(config);
   	tools.debug("Generic Websocket server created");
	}
	onHandlerClosed(handler){
		tools.debug("handler closed: " + handler.id);
		delete this.m_handlers[handler.id];
	}
	 
	onConnection(ws){
		let handler = new WebSocketHandler(this, ws, this.m_lobby);
    this.m_handlers[handler.id] = handler;
	}
	handleChangeSector(handler, msg){
		let id = msg.id;
		if (id in this.m_sectors){ 
			let sector = this.m_sectors[id];
			handler.sector = sector;
		} else {
			handler.sendError("Sector not found");
			tools.debug("client tried to join non existing sector: " +id);
		}
	} 
	handleCreateSector(host, json){
		if (!this.m_auth.checkToken(json.hostId, json.hostToken)){
			host.sendAccessDenied();
			return;
		}
		if (!json.id) {
			host.sendError("No sector id added.");
			return;
		}
		if (json.id in this.m_sectors){
			host.sendError("Sector already exists.");
			return;
		}
		let sector = new WebSocketSector(json.id, host);
		this.m_sectors[sector.id] = sector;
		sector.sectorEmpty = (sector) => this.onSectorEmpty(sector);
		tools.debug("sector created: " + json.id);
		host.sector = sector;
	}
	onSectorEmpty(sector){
		tools.debug("empty sector closed: " + sector.id);
		delete this.m_sectors[sector.id]; 
	} 
}
 
module.exports = WebSocketServer;

