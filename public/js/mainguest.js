"use strict";
import { GuestExample } from "./guestexample.js"

const localStorage = window.localStorage;
const wsp = location.protocol === "https:" ? "wss://" : "ws://";
const config = {
	endpoint: wsp + "localhost:8082/ws",
	sectorId: "ExampleSector123"
}
let guest = null;

function init(){
	guest = new GuestExample(config);
}

window.init = init;


