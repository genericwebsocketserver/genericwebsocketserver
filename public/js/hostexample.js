"use strict";
import { GenericWebSocketClient } from "./websocketclient.js";
import { tools } from "./tools.js";
import { ExampleMessageType } from "./shared.js";
export {
	HostExample
}

class Guest {
	constructor(id){
		this.m_id = id;
		this.m_name = "";
		this.m_score = 0;
		this.m_timeoutId = window.setTimeout(()=>this.onTimeout(), 1000);
		this.m_initialized = false;
	}
	get id() { return this.m_id; }
	get name() { return this.m_name; }
 	set name(value) { this.m_name = value; }
 	onTimeout(){ 
 		this.timeout();
 	}
 	timeout(){ console.log("onTimeout"); }
 	initialize(data){
 		console.debug("guest initialize: " + data.name )
 		this.m_name = data.name;
 		clearTimeout(this.m_timeoutId);
 	}
}

class HostExample{
	constructor(config){
		this.m_config = config;
		this.m_guests = {}
		this.m_gwsc = new GenericWebSocketClient(config);
		this.m_gwsc.opened = () => this.onOpened();
		this.m_gwsc.closed = () => this.onClosed();
		this.m_gwsc.clientMessage = (senderId, data) => this.onClientMessage(senderId, data);
		this.m_gwsc.clientAdded = (id) => this.onClientAdded(id);
		this.m_gwsc.clientRemoved = (id) => this.onClientRemoved(id);
		this.m_gwsc.sectorJoined = () => this.onSectorJoined();
		this.m_gwsc.openWebSocket();
		this.m_intervalId = -1;
	}
	onOpened(){
		this.m_gwsc.createSector(this.m_config.sectorId, this.m_config.hostId, this.m_config.hostToken);
		
	}
	onClosed(){
		console.debug("host ws closed reopening");
		this.m_gwsc.openWebSocket();
	}
	onClientAdded(id){
		let guest = new Guest(id);
		this.m_guests[id] = guest;
		guest.timeout = () => this.onGuestTimeout(guest);
	}
	onGuestTimeout(guest){
		console.debug("guest didn't initialize. Kicking guest");
		this.m_gwsc.kick(guest.id);
	}
	onClientRemoved(id){
		if (id in this.m_guests){
			let guest = this.m_guests[id];
			delete this.m_guests[id];
			console.debug("guest left: " + guest.name);
		}
		
	}
	onClientMessage(senderId, data){
		switch (data.t){
			case ExampleMessageType.Init:
				this.handleGuestInit(senderId, data);
				break;
		}
	}
	onSectorJoined(){
		console.log("Joined my new sector: " + this.m_gwsc.sectorId);
	}
	handleGuestInit(guestId, data){
		let guest = this.m_guests[guestId];
		guest.initialize(data);
	}
}

