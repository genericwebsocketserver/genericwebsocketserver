"use strict";

async function onLogin(e){
	let data = {
		id: document.getElementById('id').value,
		pw:	document.getElementById('pw').value
	}
	let response = await fetch("/ws/auth", {
		 method: "POST",
		 cache: "no-cache",
		 headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
	});
	if (response.status == 401){
		console.log(await response.text());
	} else if (response.status == 200){
		let token = await response.json();
		localStorage.setItem("hostId", data.id);
		localStorage.setItem("hostToken", token.id);
		window.location.href = "/";
	}
}

function init(){
	var b = document.getElementById('loginButton');
	b.onclick = onLogin;
}

window.init = init;
