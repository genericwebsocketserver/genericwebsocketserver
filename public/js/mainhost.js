"use strict";
import { HostExample } from "./hostexample.js"

const localStorage = window.localStorage;
const wsp = location.protocol === "https:" ? "wss://" : "ws://";
const config = {
	endpoint: wsp + "localhost:8082/ws",
	hostToken: localStorage.getItem("hostToken"),
	hostId: localStorage.getItem("hostId"),
	sectorId: "ExampleSector123"
}

let host = null;

function init(){
	host = new HostExample(config);
	window.host = host;
}

window.init = init;


