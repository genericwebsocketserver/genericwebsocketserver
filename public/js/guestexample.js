"use strict";

import { GenericWebSocketClient } from "./websocketclient.js"
import { tools } from "./tools.js"
import { ExampleMessageType } from "./shared.js"
export {
	GuestExample
}

class GuestExample{
	constructor(config){
		this.m_config = config;
		this.m_gwsc = new GenericWebSocketClient(config);
		this.m_gwsc.opened = () => this.onOpened();
		this.m_gwsc.closed = () => this.onClosed();
		this.m_gwsc.clientMessage = (senderId, data) => this.onClientMessage(senderId, data);
		this.m_gwsc.sectorJoined = () => this.onSectorJoined();
		this.m_gwsc.kicked = () => this.onKicked();
		this.m_gwsc.openWebSocket();
		this.m_opened = false;
		this.m_intervalId = -1;
		this.m_kicked = false;
	}
	onOpened(){
		this.m_gwsc.changeSector(this.m_config.sectorId);
		this.m_opened = true;
		let dt = 16;
		this.m_intervalId = window.setInterval(() => this.update(dt), dt);
	}
	onClosed(){
		if (this.m_kicked){
			console.debug("guest ws closed was kicked");
		} else {
			console.debug("guest ws closed reopening");
			this.m_gwsc.openWebSocket();
		}
		this.m_opened = false;
		window.clearInterval(this.m_intervalId);
	}
	update(deltaTime){
		//this.m_gwsc.sendAll(deltaTime);
	}
	onClientMessage(senderId, data){
		console.log(data);
	}
	onSectorJoined(){
		console.log("sector joined: " + this.m_gwsc.sectorId);
		let data = {
			t: ExampleMessageType.Init,
			name: "New Player"
		}
		this.m_gwsc.sendTo(this.m_gwsc.hostId, data);
	}
	onKicked(){
		this.m_kicked = true;
	}
}
