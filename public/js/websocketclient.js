"use strict";

export {
	GenericWebSocketClient
}

let MessageType = {
	ClientAdded: 1,
	ClientRemoved: 2,
	Init: 3,
	SendAll: 4,
	SendTo: 5,
	Error: 6,
	ChangeSector: 7,
	CreateSector: 8,
	CloseSector: 9,
	Kick: 10,
	AccessDenied: 11
}

class GenericWebSocketClient{
	constructor(config){
		this.m_config = config;
		this.m_ws = null;
		this.m_id = "";
		this.m_sectorId = "";
		this.m_hostId = "";
		this.m_clients = [];
   	console.debug("GenericWebSocketClient created");
	}
	get config () { return this.m_config; }
	get clients () { return this.m_clients; }
	get hostId () { return this.m_hostId; }
	get sectorId () { return this.m_sectorId; }
 	openWebSocket(){
		this.m_ws = new WebSocket(this.config.endpoint);
		this.m_id = "";
		this.m_clients = [];
		this.m_ws.onopen = (e) => this.onOpen(e); 
    this.m_ws.onclose = (e) => this.onClose(e);
    this.m_ws.onmessage = (e) => this.onMessage(e);
	}
	onOpen(e){
		console.debug("GenericWebSocketClient opened");
		this.opened();
	}
	onClose(e){
		console.debug("GenericWebSocketClient closed");
		this.m_ws.close();
		this.m_ws = null;
		this.closed();
	}
	
	async onMessage(e) {
		let msg = "";
		if (typeof e.data === "object") {
			msg = await e.data.text();
		}	else msg = e.data;
		let json = JSON.parse(msg);
		this.handleClientMessage(json);
	}

	handleClientMessage(json){
		switch (json.t){
			case MessageType.Init:
				this.handleInit(json);
				break;
			case MessageType.ClientAdded:
				if (json.id != this.m_id) this.m_clients.push(json.id);
				this.clientAdded(json.id);
				break;
			case MessageType.ClientRemoved:
				let i = this.m_clients.indexOf(json.id);
				this.m_clients.splice(i,1);
				this.clientRemoved(json.id);
				break;
			case MessageType.SendAll:
			case MessageType.SendTo:
				if (json.sender != this.m_id){
					this.clientMessage(json.sender, json.d);				
				}
				break; 
			case MessageType.AccessDenied:
				window.location.href = "./login.html";
				break;
			case MessageType.Kick:
				this.kicked();
				break;
			case MessageType.Error:
				this.error(json.msg);
				break;
			default:
				console.debug(json);
		}
	}
	handleInit(msg){
		this.m_clients = [];
		this.m_id = msg.id;
		this.m_sectorId = msg.sectorId;
		this.m_hostId = msg.hostId;
		for (let i in msg.clients){
			let client = msg.clients[i];
			if (this.m_id != client) this.m_clients.push(client);
		}
		this.sectorJoined();
	} 
	clientAdded(id) {}
	clientRemoved(id) {}
	opened() {}
	closed() {}
	sectorJoined() {}
	kicked() { console.debug("kicked"); }
	error(msg){
		console.error(msg);
	}
	clientMessage(sender, msg) {
		console.debug("message from: " + sender );
		console.debug(msg);
	}
	sendAll(data){
		let json = {
			t: MessageType.SendAll,
			d: data
		}
		this.m_ws.send(JSON.stringify(json));
	}
	sendTo(id, data){
		let json = {
			t: MessageType.SendTo,
			id: id, 
			d: data
		}
		this.m_ws.send(JSON.stringify(json));
	}
	changeSector(id){
		let json = {
			t: MessageType.ChangeSector,
			id: id
		}
		this.m_ws.send(JSON.stringify(json));
	}
	createSector(sectorId, hostId, hostToken){
		let json = {
			t: MessageType.CreateSector,
			id: sectorId,
			hostToken: hostToken,
			hostId: hostId 
		}
		this.m_ws.send(JSON.stringify(json));
	}
	kick(id){
		let json = {
			t: MessageType.Kick,
			id: id 
		}
		this.m_ws.send(JSON.stringify(json));
	}
}
